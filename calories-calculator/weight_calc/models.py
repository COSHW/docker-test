from django.db import models

# Create your models here.


class QuestionsModel(models.Model):
    question = models.CharField(max_length=500, blank=False, null=False)
    answer = models.CharField(max_length=500, blank=False, null=False)
    img = models.TextField(blank=True, null=True)


class UserModel(models.Model):
    lastName = models.CharField(max_length=100)
    firstName = models.CharField(max_length=100)
    patronymic = models.CharField(max_length=100, blank=True)
    email = models.EmailField()
    password = models.CharField(max_length=100)
    nickname = models.CharField(max_length=100)


class MenuModel(models.Model):
    dish_name = models.CharField(max_length=50)
    calories = models.IntegerField()
    img = models.TextField(blank=True, null=False, default="")
    ingredients = models.TextField()
    recipe = models.TextField()
    time_to_make = models.IntegerField()
    time_choices = (
        ('Завтрак', 'Завтрак'),
        ('Обед', 'Обед'),
        ('Ужин', 'Ужин'),
    )
    time_to_eat = models.CharField(max_length=7, choices=time_choices)


class FavMenuModel(models.Model):
    fav_menu_id = models.AutoField(primary_key=True, blank=False, null=False)
    user_id = models.ForeignKey(UserModel, on_delete=models.CASCADE)
    dish_id = models.ForeignKey(MenuModel, on_delete=models.CASCADE)


class GoalsModel(models.Model):
    user_id = models.ForeignKey(UserModel, on_delete=models.CASCADE)
    start_weight = models.IntegerField()
    start_date = models.DateField()
    daily_calories = models.IntegerField()
    current = models.BooleanField()


class DailyDataModel(models.Model):
    daily_data_id = models.AutoField(primary_key=True, blank=False, null=False)
    user_id = models.ForeignKey(GoalsModel, on_delete=models.CASCADE)
    weight = models.IntegerField()
    date = models.DateField()


class ReviewsModel(models.Model):
    review = models.CharField(max_length=1000, blank=False, null=False)
    img = models.TextField(blank=True, null=True)

