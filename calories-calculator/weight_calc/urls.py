from django.urls import path

from . import views

urlpatterns = [
    path('', views.front_page),
    path('calculator/', views.calculator),
    path('account/', views.account),
    path('questions/', views.questions),
    path('menu/', views.menu),
    path('registration/', views.registration),
    path('reviews/', views.reviews),
    path('exit/', views.unlogin),

]
