from django.shortcuts import render
from .models import QuestionsModel, UserModel, MenuModel, FavMenuModel, DailyDataModel, ReviewsModel, GoalsModel
from datetime import datetime
import plotly

# Create your views here.


def front_page(request):
    if request.method == "GET":
        current_user = login(request)
        response = render(request, 'front_page.html', {'current_user': current_user})
        if not 'current_user' in request.COOKIES:
            response.set_cookie('current_user', 'none')
    elif request.method == "POST":
        current_user = login(request)
        response = render(request, 'front_page.html', {'current_user': current_user})
        response.set_cookie('current_user', current_user)
    return response


def calculator(request):
    if request.method == "GET":
        current_user = login(request)
        response = render(request, 'calculator.html', {'current_user': current_user})
        return response
    elif request.method == "POST":
        current_user = request.COOKIES.get('current_user')
        sex = request.POST.get("sex")
        weight = request.POST.get("weight")
        height = request.POST.get("height")
        age = request.POST.get("age")
        phys = request.POST.get("phys")
        goal = request.POST.get("goal")
        goals = [-500, -250, 0, 250, 500]
        if sex == 'male':
            daily_calories = (10 * int(weight) + 6.25 * int(height) - 5 * int(age) + 5) * float(phys) + goals[int(goal)]
            perfect_weight = (int(height) - 100) * 0.9
        elif sex == 'female':
            daily_calories = (10 * int(weight) + 6.25 * int(height) - 5 * int(age) - 161) * float(phys) + goals[int(goal)]
            perfect_weight = (int(height) - 100) * 0.85
        response = render(request, 'calculation_ready.html', {'current_user': current_user, 'daily_calories': int(daily_calories), 'perfect_weight': round(perfect_weight, 2)})

        if current_user != 'none' and current_user != 'NotExists':
            user = UserModel.objects.get(nickname=current_user)
            try:
                last_goal = GoalsModel.objects.get(user_id=user.id)
                last_goal.delete()
            except:
                pass
            new_goal = GoalsModel(user_id=user, start_weight=weight, start_date=datetime.today().strftime('%Y-%m-%d'), daily_calories=daily_calories, current=True)
            new_goal.save()

        return response


def account(request):
    if request.method == "GET":
        if 'current_user' in request.COOKIES:
            current_user = request.COOKIES.get('current_user')
            if current_user != 'none' and current_user != 'NotExists':
                fav_menu = FavMenuModel.objects.filter(
                    user_id=UserModel.objects.get(nickname=request.COOKIES.get('current_user')))
                try:
                    data = DailyDataModel.objects.filter(
                        user_id=GoalsModel.objects.get(
                            user_id=UserModel.objects.get(nickname=request.COOKIES.get('current_user'))))
                    fig = plotly.graph_objs.Figure(
                        data=plotly.graph_objs.Scatter(x=[x.date for x in data], y=[y.weight for y in data]))
                    graph_div = plotly.offline.plot(fig, auto_open=False, output_type="div")
                    start_weight = GoalsModel.objects.get(user_id=UserModel.objects.get(nickname=current_user)).start_weight
                except:
                    graph_div = "Создайте цель, пройдя тестирование!"
                    start_weight = "0"
                response = render(request, 'account.html', {'current_user': current_user, 'graph_div': graph_div, 'start_weight':start_weight, 'fav_menu': fav_menu})
            else:
                response = render(request, 'front_page.html', {'current_user': "NotExists"})
    if request.method == "POST":
        new_weight = int(request.POST.get('weight'))
        new_date = request.POST.get('date')
        new_date = new_date.replace("/", '-')
        current_user = request.COOKIES.get('current_user')
        fav_menu = FavMenuModel.objects.filter(
            user_id=UserModel.objects.get(nickname=request.COOKIES.get('current_user')))
        try:
            new_daily_data = DailyDataModel(user_id=GoalsModel.objects.get(user_id=UserModel.objects.get(nickname=request.COOKIES.get('current_user'))), weight=new_weight, date=new_date)
            new_daily_data.save()

            data = DailyDataModel.objects.filter(
                user_id=GoalsModel.objects.get(
                    user_id=UserModel.objects.get(nickname=request.COOKIES.get('current_user'))))
            fig = plotly.graph_objs.Figure(
                data=plotly.graph_objs.Scatter(x=[x.date for x in data], y=[y.weight for y in data]))
            graph_div = plotly.offline.plot(fig, auto_open=False, output_type="div")
            response_message = "Спасибо, данные записаны."
            start_weight = GoalsModel.objects.get(user_id=UserModel.objects.get(nickname=current_user)).start_weight
        except:
            response_message = "Цели не существует. Пройдите тестирование!"
            graph_div = "Создайте цель, пройдя тестирование!"
            start_weight = "0"

        response = render(request, 'account.html', {'current_user': current_user, 'response': response_message, 'graph_div': graph_div, 'start_weight':start_weight, 'fav_menu': fav_menu})
    return response


def questions(request):
    if request.method == "GET":
        current_user = login(request)
        question_fields = QuestionsModel.objects.all()
        response = render(request, 'questions.html', {'current_user': current_user, 'question_fields': question_fields})

    elif request.method == "POST":
        current_user = login(request)
        response = render(request, 'questions.html', {'current_user': current_user})
        response.set_cookie('current_user', current_user)
    return response


def reviews(request):
    if request.method == "GET":
        current_user = login(request)
        review_fields = ReviewsModel.objects.all()
        response = render(request, 'reviews.html', {'current_user': current_user, 'review_fields': review_fields})

    elif request.method == "POST":
        current_user = login(request)
        review_fields = ReviewsModel.objects.all()
        response = render(request, 'reviews.html', {'current_user': current_user, 'review_fields': review_fields})
        response.set_cookie('current_user', current_user)
    return response


def menu(request):
    current_user = request.COOKIES.get('current_user')
    zav = MenuModel.objects.filter(time_to_eat="Завтрак")
    obed = MenuModel.objects.filter(time_to_eat="Обед")
    ujin = MenuModel.objects.filter(time_to_eat="Ужин")
    try:
        calories = GoalsModel.objects.get(user_id=UserModel.objects.get(nickname=current_user)).daily_calories
    except:
        calories = 0
    if request.method == "POST":
        fav = FavMenuModel(user_id=UserModel.objects.get(nickname=current_user), dish_id=MenuModel.objects.get(id=request.POST.get('fav_id')))
        fav.save()
    return render(request, 'menu.html', {'current_user': current_user, 'zav': zav, 'obed': obed, 'ujin': ujin,
                                             'cal': calories})


def registration(request):
    if request.method == "GET":
        if 'current_user' in request.COOKIES:
            current_user = request.COOKIES.get('current_user')
            if current_user == 'none' or current_user == 'NotExists':
                response = render(request, 'registration.html', {'current_user': 'none'})
            else:
                response = render(request, 'front_page.html', {'current_user': current_user})
    elif request.method == "POST":
        firstName = request.POST.get('firstName')
        lastName = request.POST.get('lastName')
        patro = request.POST.get('patro')
        nick = request.POST.get('nick')
        reg_email = request.POST.get('email_reg')
        reg_password = request.POST.get('psw_reg')
        if len(UserModel.objects.filter(email=reg_email)) > 0:
            response = render(request, 'registration.html', {'current_user': 'none', 'email_exists': True})
        elif len(UserModel.objects.filter(nickname=nick)) > 0:
            response = render(request, 'registration.html', {'current_user': 'none', 'nick_exists': True})
        else:
            new_user = UserModel(lastName=lastName, firstName=firstName, patronymic=patro, email=reg_email, password=reg_password, nickname=nick)
            new_user.save()
            response = render(request, 'front_page.html', {'current_user': nick})
            response.set_cookie('current_user', nick)
    return response


def login(request):
    if request.method == "GET":
        if 'current_user' in request.COOKIES:
            current_user = request.COOKIES.get('current_user')
            print('cookies user for ' + current_user)
        else:
            current_user = 'none'
    elif request.method == "POST":
        current_user_email = request.POST.get('email')
        current_user_password = request.POST.get('password')
        try:
            user = UserModel.objects.get(email=current_user_email, password=current_user_password)
            current_user = user.nickname
        except UserModel.DoesNotExist:
            current_user = 'NotExists'

    return current_user


def unlogin(request):
    response = render(request, 'front_page.html', {'current_user': 'none'})
    response.set_cookie('current_user', 'none')

    if request.method == "POST":
        current_user = login(request)
        response = render(request, 'front_page.html', {'current_user': current_user})
        response.set_cookie('current_user', current_user)
    return response

