from django.contrib import admin

# Register your models here.
from .models import QuestionsModel, UserModel, MenuModel, FavMenuModel, DailyDataModel, ReviewsModel, GoalsModel

admin.site.register(QuestionsModel)
admin.site.register(UserModel)
admin.site.register(MenuModel)
admin.site.register(FavMenuModel)
admin.site.register(DailyDataModel)
admin.site.register(ReviewsModel)
admin.site.register(GoalsModel)
