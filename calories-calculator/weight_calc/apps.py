from django.apps import AppConfig


class WeightCalcConfig(AppConfig):
    name = 'weight_calc'
