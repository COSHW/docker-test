from django.db import models

# Create your models here.


class ProductTypesModel(models.Model):
    product_type = models.TextField()


class ProductModel(models.Model):
    product_name = models.TextField()
    product_type = models.ForeignKey(ProductTypesModel, on_delete=models.CASCADE)


class RequestsModel(models.Model):
    fio = models.TextField()
    company_name = models.TextField()
    location = models.TextField()
    position = models.TextField(blank=True, default='')
    activity_area = models.TextField(blank=True, default='')
    email = models.TextField()
    phone = models.TextField()
    description = models.TextField(blank=True, default='')
    accepted = models.NullBooleanField(blank=True, null=True, default=None,)
    product_type = models.ForeignKey(ProductModel, on_delete=models.CASCADE)
