from django.contrib import admin

# Register your models here.
from .models import ProductModel, ProductTypesModel, RequestsModel

admin.site.register(ProductModel)
admin.site.register(ProductTypesModel)
admin.site.register(RequestsModel)
