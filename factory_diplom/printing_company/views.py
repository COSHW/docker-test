from django.shortcuts import render
from django.http import HttpResponse
from .models import RequestsModel, ProductModel, ProductTypesModel
# Create your views here.


def front_page(request):
    return render(request, 'start_page.html')


def about(request):
    return render(request, 'company/about.html')


def history(request):
    return render(request, 'company/history.html')


def cvodka(request):
    return render(request, 'company/cvodka.html')


def policy(request):
    return render(request, 'company/policy.html')


def reviews(request):
    return render(request, 'company/reviews.html')


def tech(request):
    return render(request, 'company/tech.html')


def bilet_1(request):
    return render(request, 'products/bileti/1.html')


def bilet_2(request):
    return render(request, 'products/bileti/2.html')


def bilet_3(request):
    return render(request, 'products/bileti/3.html')


def etiketki_1(request):
    return render(request, 'products/etiketki/1.html')


def etiketki_2(request):
    return render(request, 'products/etiketki/2.html')


def etiketki_3(request):
    return render(request, 'products/etiketki/3.html')


def etiketki_4(request):
    return render(request, 'products/etiketki/4.html')


def etiketki_5(request):
    return render(request, 'products/etiketki/5.html')


def etiketki_6(request):
    return render(request, 'products/etiketki/6.html')


def etiketki_7(request):
    return render(request, 'products/etiketki/7.html')


def etiketki_8(request):
    return render(request, 'products/etiketki/8.html')


def polyproducts_1(request):
    return render(request, 'products/polyproducts/1.html')


def polyproducts_2(request):
    return render(request, 'products/polyproducts/2.html')


def polyproducts_3(request):
    return render(request, 'products/polyproducts/3.html')


def polyproducts_4(request):
    return render(request, 'products/polyproducts/4.html')


def polyproducts_5(request):
    return render(request, 'products/polyproducts/5.html')


def polyproducts_6(request):
    return render(request, 'products/polyproducts/6.html')


def polyproducts_7(request):
    return render(request, 'products/polyproducts/7.html')


def polyproducts_8(request):
    return render(request, 'products/polyproducts/8.html')


def polyproducts_9(request):
    return render(request, 'products/polyproducts/9.html')


def polyproducts_10(request):
    return render(request, 'products/polyproducts/10.html')


def polyproducts_11(request):
    return render(request, 'products/polyproducts/11.html')


def polyproducts_12(request):
    return render(request, 'products/polyproducts/12.html')


def polyproducts_13(request):
    return render(request, 'products/polyproducts/13.html')


def send_request(request):
    if request.method == "GET":
        types = ProductTypesModel.objects.all()
        products = {}
        for t in types:
            products[t.product_type] = []
            prod_list = ProductModel.objects.filter(product_type=t)
            for item in prod_list:
                products[t.product_type].append(item)
        response = render(request, 'send_request.html', {'products': products})

    elif request.method == "POST":
        types = ProductTypesModel.objects.all()
        products = {}
        for t in types:
            products[t.product_type] = []
            prod_list = ProductModel.objects.filter(product_type=t)
            for item in prod_list:
                products[t.product_type].append(item)
        if request.POST.get('form_select') == "choice":
            response = render(request, 'send_request.html', {'products': products, 'empty_choice': True})
        else:
            fio = request.POST.get('form_text_1')
            company_name = request.POST.get('form_text_2')
            location = request.POST.get('form_text_3')
            position = request.POST.get('form_text_4')
            activity_area = request.POST.get('form_text_5')
            email = request.POST.get('form_text_6')
            phone = request.POST.get('form_text_7')
            description = request.POST.get('form_textarea_11')
            accepted = None
            product_type = ProductModel.objects.filter(product_name=request.POST.get('form_select'))[0]
            new_row = RequestsModel(fio=fio, company_name=company_name, location=location, position=position, activity_area=activity_area, email=email, phone=phone, description=description, accepted=accepted, product_type=product_type)
            new_row.save()

            response = render(request, 'send_request.html', {'products': products, 'is_sent': True})
    else:
        response = HttpResponse("Недопустимый метод!")

    return response


def site_map(request):
    return render(request, 'site_map.html')


def contacts(request):
    return render(request, 'contacts.html')
