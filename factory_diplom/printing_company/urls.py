from django.urls import path

from . import views

urlpatterns = [
    path('', views.front_page),
    path('about/', views.about),
    path('history/', views.history),
    path('cvodka/', views.cvodka),
    path('policy/', views.policy),
    path('reviews/', views.reviews),
    path('tech/', views.tech),
    path('send_request/', views.send_request),
    path('map/', views.site_map),
    path('contacts/', views.contacts),

    path('bilet1/', views.bilet_1),
    path('bilet2/', views.bilet_2),
    path('bilet3/', views.bilet_3),

    path('etiketki1/', views.etiketki_1),
    path('etiketki2/', views.etiketki_2),
    path('etiketki3/', views.etiketki_3),
    path('etiketki4/', views.etiketki_4),
    path('etiketki5/', views.etiketki_5),
    path('etiketki6/', views.etiketki_6),
    path('etiketki7/', views.etiketki_7),
    path('etiketki8/', views.etiketki_8),

    path('polyproducts1/', views.polyproducts_1),
    path('polyproducts2/', views.polyproducts_2),
    path('polyproducts3/', views.polyproducts_3),
    path('polyproducts4/', views.polyproducts_4),
    path('polyproducts5/', views.polyproducts_5),
    path('polyproducts6/', views.polyproducts_6),
    path('polyproducts7/', views.polyproducts_7),
    path('polyproducts8/', views.polyproducts_8),
    path('polyproducts9/', views.polyproducts_9),
    path('polyproducts10/', views.polyproducts_10),
    path('polyproducts11/', views.polyproducts_11),
    path('polyproducts12/', views.polyproducts_12),
    path('polyproducts13/', views.polyproducts_13),

]
