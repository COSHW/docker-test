from django.apps import AppConfig


class PrintingCompanyConfig(AppConfig):
    name = 'printing_company'
